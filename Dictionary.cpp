#include "Dictionary.h"

#include <thread>
#include <fstream>
#include <string>
#include <algorithm>
#include <iomanip>

#undef min
#undef max

// PREDEFINED FUNCTIONS
bool IsEmptyLine(std::string& line);
bool CheckDelimiter(std::string& entrySection, std::string& line, std::string& wordDelimiter, std::string& idxDelimiter);
bool CheckDelimiterIndex(std::string& subString, std::string& idxDelimiter);
bool CheckDelimiterString(std::string& subString, std::string& wordDelimiter);


bool Dictionary::Setup(HANDLE hConsole)
{
	console = hConsole;

	// PREPARE INI DATA
	std::ifstream inFile{ "IniData.txt" };
	if (inFile.is_open())
	{
		while (!inFile.eof())
		{
			std::string line{};
			std::getline(inFile, line);

			if (!IsEmptyLine(line))
			{
				unsigned count = std::stoi(line);
				currentWordCount = count;
			}
		}
	}
	else
	{
		std::cout << "<<< FILE COULD NOT BE OPENED >>>" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(2));
		return false;
	}

	return true;
}

bool Dictionary::MainLoop()
{
	// First thing to do in the loop is always to lear the screen
	cls();
	// and then display the main input options and ask for input
	DisplayMainOptions();

	return bQuit;
}

void Dictionary::DisplayMainOptions()
{
	std::cout << "====================================================================================================" << std::endl;
	std::cout << std::endl;
	std::cout << "F - Find a word" << std::endl;
	std::cout << "A - Add a word" << std::endl;
	std::cout << "Q - Quit" << std::endl;
	std::cout << std::endl;
	std::cout << "Type in your input... ";

	std::string keyString{};
	std::getline(std::cin, keyString);
	std::cout << std::endl;

	char key = keyString[0];

	CharToUpper(key);

	ProcessMainInput(key);
}

void  Dictionary::ProcessMainInput(char key)
{
	if (key == 'F')
		FindWords();
	else if (key == 'A')
		AddWord();
	else if (key == 'Q')
		bQuit = true;
	else
	{
		std::cout << "<<< Input was invalid. Try again. >>>" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(2));
	}
}

void Dictionary::FindWords()
{
	fittingEntries.clear();

	std::cout << "Type in any characters to search for and press Enter... ";
	std::string charactersToLookFor{ "" };
	getline(std::cin, charactersToLookFor);

	// OPEN FILE IF IT EXISTS
	std::ifstream inFile{ "Dictionary.txt" };
	// READ EACH LINE FROM IT AND COMPARE IT WITH THE INPUT
	if (inFile.is_open())
	{
		while (!inFile.eof())
		{
			Entry newEntry{};

			// TRY TO FIND A FULL ENTRY
			std::string line{};
			std::vector<std::string> entryLines{};
			std::getline(inFile, line);

			// try to find the Index Delimiter
			if (!IsEmptyLine(line))
			{
				// get the first line, which should be only a number
				unsigned start = line.find_first_of('[');
				unsigned last = line.find_first_of(']');
				std::string idxDelimiter = line.substr(start + 1, last - start -1);

				// THIS THROWS AN EXEPTION IF IT FAILS
				std::stoi(idxDelimiter);

				// get 3 lines for the word, translation and description
				for (size_t i = 0; i < 3; i++)
				{
					std::getline(inFile, line);
					entryLines.push_back(line);
				}

				if (!ConvertLinesToEntry(newEntry, entryLines, idxDelimiter))
					continue;
			}


			// if we find a valid entry add it to the entries vector
			if (newEntry.word.find(charactersToLookFor) != std::string::npos)
			{
				fittingEntries.push_back(newEntry);
			}
		}
	}
	else
	{
		std::cout << "<<< FILE COULD NOT BE OPENED >>>" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(2));
	}
	// CLOSE FILE
	inFile.close();

	if (fittingEntries.size() <= 0)
	{
		std::cout << "<<< THERE ARE NO FITTING ENTRIES IN THE DICTIONARY! >>>" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(2));

		return;
	}

	std::cout << std::endl;

	DisplayFoundWords();
}

bool IsEmptyLine(std::string& line)
{
	if (line.empty())
		return true;

	// this checks if the string only consits of whitespaces and a terminator
	for (auto c : line)
	{
		if (c != ' ')
			return false;
	}

	return true;
}

void Dictionary::DisplayFoundWords()
{
	std::cout << "====================================================================================================" << std::endl;
	std::cout << std::endl;

	for (size_t i = 0; i < fittingEntries.size(); i++)
	{
		std::cout << "____________________________________________________________________________________________________" << std::endl
			<< "| " << std::setw(5) << std::left << i
			<< " | " << std::setw(50) << std::left << fittingEntries.at(i).word
			<< " | " << fittingEntries.at(i).translation << std::endl;
	}

	std::cout << std::endl << "To select a word for further information press S..." << std::endl;
	std::cout << "If you want to go back press anything else." << std::endl;
	std::string keyString{ "" };
	getline(std::cin, keyString);

	if (keyString.size() <= 0)
		return;

	char key = keyString.at(0);
	CharToUpper(key);

	if (key == 'S')
		SelectWordAtIndex();
}

void Dictionary::CharToUpper(char& lower)
{
	for (size_t i = 0; i < lowerChars.size(); i++)
	{
		if (lower == lowerChars[i])
			lower = upperChars[i];
	}
}

void Dictionary::AddWord()
{
	Entry newEntry{};

	std::cout << "===========================================================================================" << std::endl;
	std::cout << std::endl;
	std::cout << "Type in the name of the word... ";
	std::getline(std::cin, newEntry.word);
	std::cout << std::endl;
	std::cout << "Type in the german translations... ";
	std::getline(std::cin, newEntry.translation);
	std::cout << std::endl;
	std::cout << "Type in the english description... ";
	std::getline(std::cin, newEntry.englishDescription);
	std::cout << std::endl;

	std::ifstream inFile{ "Dictionary.txt" };
	std::vector<std::string> linesToCopy{};

	// COPY ALL EXISTING LINES
	if (inFile.is_open())
	{
		while (!inFile.eof())
		{
			std::string line{};
			std::getline(inFile, line);

			if (!IsEmptyLine(line))
				linesToCopy.push_back(line);
		}
	}
	else
	{
		std::cout << "<<< FILE COULD NOT BE OPENED >>>" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(2));
	}
	// CLOSE FILE
	inFile.close();

	std::ofstream outFile{ "Dictionary.txt" };
	std::ofstream iniFile{ "IniData.txt" };
	// ADD NEW ONES
	if (outFile.is_open() && iniFile.is_open())
	{
		std::vector<std::string> newLines{};
		ConvertEntryToLines(newEntry, newLines);
		for (auto line : newLines)
			linesToCopy.push_back(line);

		for (auto line : linesToCopy)
			outFile << line << std::endl;

		currentWordCount++;

		iniFile << currentWordCount;
	}
	else
	{
		std::cout << "<<< FILES COULD NOT BE OPENED >>>" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(2));
	}
	// CLOSE FILE
	outFile.close();
}

void Dictionary::SelectWordAtIndex()
{
	std::cout << "Type in a valid index...";
	unsigned idx{ 0 };
	std::cin >> idx;
	std::cin.clear();
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	if (idx < 0 || idx > fittingEntries.size() - 1)
	{
		std::cout << "<<< INDEX IS INVALID >>>" << fittingEntries.size() - 1 << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(2));
	}
	else
	{
		cls();
		currentSelectedWord = fittingEntries.at(idx);
		DisplaySelectedWordInfo();
	}
}

void Dictionary::DisplaySelectedWordInfo()
{
	std::cout << "==============================================================================================================" << std::endl;
	std::cout << std::setw(50) << std::left << currentSelectedWord.word
		<< " | " << currentSelectedWord.translation
		<< std::endl
		<< std::endl
		<< currentSelectedWord.englishDescription
		<< std::endl;

	//std::cout << std::endl << "To select a word for further information press S..." << std::endl;
	//std::cout << "If you want to go back press anything else." << std::endl;
	std::cout << std::endl << "Press anything to go back." << std::endl;
	std::string keyString{ "" };
	getline(std::cin, keyString);

	if (keyString.size() <= 0)
		return;

	char key = keyString.at(0);
	CharToUpper(key);

	ProcessSelectedWordInput(key);
}

void Dictionary::ProcessSelectedWordInput(char key)
{
	//if (key == 'F')
	//	FindWords();
	//else if (key == 'A')
	//	AddWord();
	//else if (key == 'Q')
	//	bQuit = true;
	//else
	//{
	//	std::cout << "<<< INVALID INPUT >>>" << std::endl;
	//	std::this_thread::sleep_for(std::chrono::seconds(2));
	//}

	DisplayFoundWords();
}

bool Dictionary::ConvertLinesToEntry(Entry& inEntry, std::vector<std::string>& inLines, std::string& �dxDelimiter)
{
	std::string wordDelimiter{};

	// WORD
	wordDelimiter = "WORD";
	if (!CheckDelimiter(inEntry.word, inLines.at(0), wordDelimiter, �dxDelimiter))
		return false;

	// TRANSLATION
	wordDelimiter = "TRANSLATION";
	if (!CheckDelimiter(inEntry.translation, inLines.at(1), wordDelimiter, �dxDelimiter))
		return false;


	// DESCRIPTION
	wordDelimiter = "DESCRIPTION";
	if (!CheckDelimiter(inEntry.englishDescription, inLines.at(2), wordDelimiter, �dxDelimiter))
		return false;

	return true;
}

bool CheckDelimiter(std::string& entrySection, std::string& line, std::string& wordDelimiter, std::string& idxDelimiter)
{
	unsigned start{};
	unsigned last{};

	start = line.find_first_of('[');
	last = line.find_first_of(']');
	std::string subString = line.substr(start, last - start);

	if (CheckDelimiterIndex(subString, idxDelimiter))
	{
		if (CheckDelimiterString(subString, wordDelimiter))
			entrySection = line.substr(last + 1, line.size() - 1);
		else
			return false;
	}
	else
		return false;
}

bool CheckDelimiterIndex(std::string& subString, std::string& idxDelimiter)
{
	unsigned start{};
	unsigned last{};

	start = subString.find_first_of('[');
	last = subString.find_first_of('|');
	std::string subStr = subString.substr(start + 1, last - start - 1);

	if (subStr == idxDelimiter)
		return true;
	else
	{
		std::cout << "<<< INDEX DELIMITER WRONG >>>" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(2));
		return false;
	}
}

bool CheckDelimiterString(std::string& subString, std::string& wordDelimiter)
{
	unsigned start{};
	unsigned last{};

	start = subString.find_first_of('|');
	last = subString.find_first_of(']');
	std::string subStr = subString.substr(start + 1, last - start - 1);

	if (subStr == wordDelimiter)
		return true;
	else
	{
		std::cout << "<<< WORD DELIMITER WRONG >>>" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(2));
		return false;
	}
}

void Dictionary::ConvertEntryToLines(Entry& inEntry, std::vector<std::string>& inLines)
{
	inLines.push_back("[" + std::to_string(currentWordCount + 1) + "]");
	inLines.push_back("[" + std::to_string(currentWordCount + 1) + "|WORD]" + inEntry.word);
	inLines.push_back("[" + std::to_string(currentWordCount + 1) + "|TRANSLATION]" + inEntry.translation);
	inLines.push_back("[" + std::to_string(currentWordCount + 1) + "|DESCRIPTION]" + inEntry.englishDescription);
}

void Dictionary::SortFoundEntries()
{
	std::sort(fittingEntries.begin(), fittingEntries.end(), [](const Entry& lhs, const Entry& rhs) {
		return lhs.word < rhs.word;
		});
}

void Dictionary::cls()
{
	COORD coordScreen = { 0, 0 };    // home for the cursor 
	DWORD cCharsWritten;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD dwConSize;

	// Get the number of character cells in the current buffer. 

	if (!GetConsoleScreenBufferInfo(console, &csbi))
	{
		return;
	}

	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

	// Fill the entire screen with blanks.

	if (!FillConsoleOutputCharacter(console,        // Handle to console screen buffer 
		(TCHAR)' ',     // Character to write to the buffer
		dwConSize,       // Number of cells to write 
		coordScreen,     // Coordinates of first cell 
		&cCharsWritten))// Receive number of characters written
	{
		return;
	}

	// Get the current text attribute.

	if (!GetConsoleScreenBufferInfo(console, &csbi))
	{
		return;
	}

	// Set the buffer's attributes accordingly.

	if (!FillConsoleOutputAttribute(console,         // Handle to console screen buffer 
		csbi.wAttributes, // Character attributes to use
		dwConSize,        // Number of cells to set attribute 
		coordScreen,      // Coordinates of first cell 
		&cCharsWritten)) // Receive number of characters written
	{
		return;
	}

	// Put the cursor at its home coordinates.

	SetConsoleCursorPosition(console, coordScreen);
}