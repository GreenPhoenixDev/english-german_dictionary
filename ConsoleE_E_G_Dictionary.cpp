// ConsoleE_E_G_Dictionary.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "Dictionary.h"

int main()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	Dictionary dict;
	if (!dict.Setup(hConsole))
		return 1;

	bool bQuit{ false };

	while (!bQuit)
	{
		bQuit = dict.MainLoop();
	}

	return 0;
}